package com.hxh.unified.param.check.example.doc.v1;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/23 3:11 下午
 * Utils: Intellij Idea
 * Description:
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DocFormValidator.class)
public @interface DocFormAnnotation {

    String message() default "文档内容数据有误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}