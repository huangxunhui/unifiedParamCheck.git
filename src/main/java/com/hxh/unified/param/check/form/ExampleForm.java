package com.hxh.unified.param.check.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 2020/3/4 9:31 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class ExampleForm {

    @NotEmpty(message = "姓名不能为空")
    @Length(min = 1 , max = 10 , message = "名字长度1~10")
    private String name;

    @Range(min = 1 , max = 99 , message = "年龄范围在1~99岁")
    private Integer age;

    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$" , message = "电话号码有误")
    private String phone;

    @Email(message = "邮箱格式有误")
    private String email;

    @Valid
    @Size(min = 1 ,max =  10 , message = "列表中的元素数量为1~10")
    private List<RequestForm> requestFormList;

    @Future(message = "开始时间必须大于当前时间")
    private Date beginTime;

}
