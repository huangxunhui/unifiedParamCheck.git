package com.hxh.unified.param.check.form;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

/**
 * @author huangxunhui
 * Date: Created in 2020/3/4 6:13 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class RequestForm {

    @NotEmpty(message = "姓名不能为空")
    private String name;

    @Min(value = 1 , message = "年龄不能小于1岁")
    private Integer age;

    @NotEmpty(message = "性别不能为空")
    private String sex;

}
