/*
 * Copyright (c) 2015-2019 广东口袋零钱信息技术有限公司 All Rights Reserved.
 */

package com.hxh.unified.param.check.vo;

import lombok.Data;

/**
 * @author huangxunhui
 * @date Created in 2020/3/4 6:09 下午
 * Utils: Intellij Idea
 * Description: 固定返回格式
 */
@Data
public class ResultVo {

    /**
     * 错误码
     */
    private String code;

    /**
     * 提示信息
     */
    private String message;

    /**
     * 具体的内容
     */
    private Object data;

}
