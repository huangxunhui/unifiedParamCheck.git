package com.hxh.unified.param.check.example.doc.v2;

import lombok.Data;
import org.hibernate.validator.group.GroupSequenceProvider;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/23 11:33 上午
 * Utils: Intellij Idea
 * Description:
 */
@Data
@GroupSequenceProvider(value = DocSequenceProvider.class)
public class V2DocForm {

    /**
     * 文档类型
     */
    @NotEmpty(message = "文档类型不能为空")
    @Pattern(regexp = "^(phone|idCard|bankCard)$", message = "文档类型有误 phone, idCard, bankCard")
    private String docType;

    /**
     * 文档内容
     */
    @NotEmpty(message = "文档内容不能为空")
    @Pattern(regexp = "^\\d{15}|\\d{18}$" ,message = "身份证有误", groups = {idCard.class})
    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$" ,message = "手机号码有误", groups = {phone.class})
    @Pattern(regexp = "^^[1-9]\\d{12,18}$$" ,message = "银行卡有误",groups = {bankCard.class})
    private String docContent;

    public interface idCard{}

    public interface phone{}

    public interface bankCard{}



}
