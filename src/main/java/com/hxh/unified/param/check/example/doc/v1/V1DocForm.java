package com.hxh.unified.param.check.example.doc.v1;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/23 11:33 上午
 * Utils: Intellij Idea
 * Description:
 */
@Data
@DocFormAnnotation
public class V1DocForm {

    /**
     * 文档类型
     */
    @NotEmpty(message = "类型不能为空")
    @Pattern(regexp = "^(phone|idCard|bankCard)$", message = "类型有误-类型可选[phone|idCard|bankCard]")
    private String docType;

    /**
     * 文档内容
     */
    @NotEmpty(message = "内容不能为空")
    private String docContent;

}
