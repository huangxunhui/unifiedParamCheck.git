package com.hxh.unified.param.check.form;

import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/21 6:31 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class PhoneForm {

    /**
     * 电话号码
     */
    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$" , message = "电话号码有误")
    private String phone;

}
