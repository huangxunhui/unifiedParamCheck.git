![](https://cos.huangxunhui.com/blog/unified_param_check/unified_param_check_header.jpg)

### 前言

&emsp;&emsp;在日常的开发中，**参数校验**是非常重要的一个环节，严格参数校验会减少很多出bug的概率，增加接口的安全性。也会减少对接时不必要的沟通。比如说：在对接的时候前端动不动就甩个截图过来说接口有问题，你检查了半天发现前端传递的参数有问题。针对以上：今天给大家分享一下**SpringBoot**如何实现统一参数校验。

### 实现方式

&emsp;&emsp;使用 ```@Validated```注解配合参数校验注解， 比如：```@NotEmpty```对参数进行校验。然后对抛出的异常```ControllerAdvice```进行捕获然后调整输出数据。

### TestController

```java
@RestController
public class TestController {

    /**
     * 表单请求
     * @param form 请求参数
     * @return 响应数据
     */
    @PostMapping("/formRequest")
    public ResultVo formRequest(@Validated RequestForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * JSON请求
     * @param form 请求参数
     * @return 响应数据
     */
    @PostMapping("/jsonRequest")
    public ResultVo jsonRequest(@RequestBody @Validated RequestForm form){
        return ResultVoUtil.success(form);
    }

}
```

### RequestForm

```java
@Data
public class RequestForm {

    @NotEmpty(message = "姓名不能为空")
    private String name;

    @Min(value = 1 , message = "年龄不能小于1岁")
    private Integer age;

    @NotEmpty(message = "性别不能为空")
    private Integer sex;

}
```

### 测试结果

请求：http://localhost:8080/formRequest 不传任何参数。

![](https://cos.huangxunhui.com/blog/unified_param_check/form_request.png)

&emsp;&emsp;这个时候SpringBoot已经根据校验注解对参数进行校验了。并且输出了一大堆的错误信息。这个时候前端在对接的时候看到这样的错误信息，反手就是给你截个图告诉你接口有问题。所以这个时候就该使用 ```ControllerAdvice```规范异常返回信息了。

![image](https://cos.huangxunhui.com/blog/unified_param_check/has_problem.jpg)


### ControllerAdvice

```java
@Slf4j
@RestControllerAdvice
public class ControllerAdvice {

    /**
     * 拦截表单参数校验
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({BindException.class})
    public ResultVo bindException(BindException e) {
        BindingResult bindingResult = e.getBindingResult();
        return ResultVoUtil.error(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
    }
    
    /**
     * 拦截JSON参数校验
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResultVo bindException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        return ResultVoUtil.error(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
    }

}
```

 **@RestControllerAdvice会将返回的数据以json输出，如果不需要可以使用@ControllerAdvice**

> 以上代码只返回了错误信息。可以根据实际情况进行调整。

![image](https://cos.huangxunhui.com/blog/unified_param_check/form_request_result.png)

这个时候的错误信息就比较友好了，非常明确的指出了缺少参数。


### 常用校验注解

| 注解                                           | 运行时检查                                                   |
| ---------------------------------------------- | ------------------------------------------------------------ |
| @AssertFalse                                   | 被注解的元素必须为false                                      |
| @AssertTrue                                    | 被注解的元素必须为true                                       |
| @DecimalMax(value)                             | 被注解的元素必须为一个数字，其值必须小于等于指定的最小值     |
| @DecimalMin(Value)                             | 被注解的元素必须为一个数字，其值必须大于等于指定的最小值     |
| @Digits(integer=, fraction=)                   | 被注解的元素必须为一个数字，其值必须在可接受的范围内         |
| @Future                                        | 被注解的元素必须是日期，检查给定的日期是否比现在晚           |
| @Max(value)                                    | 被注解的元素必须为一个数字，其值必须小于等于指定的最小值     |
| @Min(value)                                    | 被注解的元素必须为一个数字，其值必须大于等于指定的最小值     |
| @NotNull                                       | 被注解的元素必须不为null                                     |
| @Null                                          | 被注解的元素必须为null                                       |
| @Past(java.util.Date/Calendar)                 | 被注解的元素必须过去的日期，检查标注对象中的值表示的日期比当前早 |
| @Pattern(regex=, flag=)                        | 被注解的元素必须符合正则表达式，检查该字符串是否能够在match指定的情况下被regex定义的正则表达式匹配 |
| @Size(min=, max=)                              | 被注解的元素必须在制定的范围(数据类型:String, Collection, Map and arrays) |
| @Valid                                         | 递归的对关联对象进行校验, 如果关联对象是个集合或者数组, 那么对其中的元素进行递归校验,如果是一个map,则对其中的值部分进行校验 |
| @CreditCardNumber                              | 对信用卡号进行一个大致的验证                                 |
| @Email                                         | 被注释的元素必须是电子邮箱地址                               |
| @Length(min=, max=)                            | 被注解的对象必须是字符串的大小必须在制定的范围内             |
| @NotBlank                                      | 被注解的对象必须为字符串，不能为空，检查时会将空格忽略       |
| @NotEmpty                                      | 被注释的对象必须为空(数据:String,Collection,Map,arrays)      |
| @Range(min=, max=)                             | 被注释的元素必须在合适的范围内 (数据：BigDecimal, BigInteger, String, byte, short, int, long and 原始类型的包装类 ) |
| @URL(protocol=, host=, port=, regexp=, flags=) | 被注解的对象必须是字符串，检查是否是一个有效的URL，如果提供了protocol，host等，则该URL还需满足提供的条件 |

### 案例

```java
@Data
public class ExampleForm {

    @NotEmpty(message = "姓名不能为空")
    @Length(min = 1 , max = 10 , message = "名字长度1~10")
    private String name;

    @Range(min = 1 , max = 99 , message = "年龄范围在1~99岁")
    private Integer age;

    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$" , message = "电话号码有误")
    private String phone;

    @Email(message = "邮箱格式有误")
    private String email;

    @Valid
    @Size(min = 1 ,max =  10 , message = "列表中的元素数量为1~10")
    private List<RequestForm> requestFormList;

    @Future(message = "开始时间必须大于当前时间")
    private Date beginTime;
    
}
```

### 实现嵌套验证

&emsp;&emsp;在实际的开发中，前台会后台传递一个list，我们不仅要限制每次请求list内的个数，同时还要对list内基本元素的属性值进行校验。这个时候就需要进行嵌套验证了，实现的方式很简单。在list上添加@Vaild就可以实现了。

```java
@Data
public class JsonRequestForm {
    
    @Vaild
    @Size(min = 1 ,max =  10 , message = "列表中的元素数量为1~10")
    private List<RequestForm> requestFormList;
    
}
```

### 代码地址

> https://gitee.com/huangxunhui/unifiedParamCheck.git 

### 结尾

&emsp;&emsp;如果觉得对你有帮助，可以多多评论，多多点赞哦，也可以到我的主页看看，说不定有你喜欢的文章，也可以随手点个关注哦，谢谢。

![](https://cos.huangxunhui.com/blog/个人铭牌.png)